<?php # Script 3.10 - calculator.php #5

$page_title = 'Widget Cost Calculator';
include('includes/header.html');

function calculate_total ($qty, $cost, $tax = 5) {
	$total = ($qty * $cost);
	$taxrate = ($tax /100);
	$total += ($total * $taxrate);
	
	return number_format($total, 2);
}

// Check for form submission:
if (isset($_POST['submitted'])) {

	// Cast all the variables to a specific type
	$quantity = (int) $_POST['quantity'];
	$price = (float) $_POST['price'];
	$tax = (float) $_POST['tax'];
	// Minimal form validation
	if (is_numeric($quantity) && is_numeric($price)) {
		// Print the results:
		echo '<h1>Total Cost:</h1>';
		if (is_numeric($tax)){
			$sum = calculate_total($quantity, $price, $tax);
		} else {
			$sum = calculate_total($quantity,$price);
		}
		echo '<p>The total cost of purchasing ' . $quantity . ' widget(s) at $' . number_format($price, 2) . ' each, including tax, is $' . $sum . '.</p>';
	} else { // Invalid submitted values
		echo '<h1>Error!</h1>
		<p class="error">Please enter a valid quantity, price, and tax.</p>';
	}
} // End of main isset() IF.
?>

<h1>Widget Cost Calculator</h1>
<form action="calculator.php" method="post">
	<p>Quantity: <input name="quantity" type="text" size="5" maxlength="5" value="<?php if (isset($quantity)) echo $quantity; ?>"/></p>
	
	<p>Price: <input name="price" type="text" size="5" maxlength="10"value="<?php if (isset($price)) echo $price; ?>"/></p>
	
	<p>Tax (%): <input name="tax" type="text" size="5" maxlength="5"value="<?php if (isset($tax)) echo $tax; ?>"/> (optional)</p>
	
	<p><input name="submit" type="image" src="http://www.legacyrealtyinc.com/images/siteButton-calculate-down.png" value="Calculate" /></p>
	
	<input name="submitted" type="hidden" value="1"/>
</form>

<?php // Include footer
include ('includes/footer.html');
?>