<?php # Script 10.3 - upload_image.php

// Retrieves all the records from the users table
// This new version links to edit and delete pages.

$page_title = 'Upload Image';
include('includes/header.html');
include ('includes/report_errors.php');
echo '<h1>Upload Image</h1>';

if (isset($_POST['submitted'])) {
	if (isset($_FILES['upload'])) {
		// Validate the type
		$allowed = array ('image/pjpeg', 'image/jpeg', 'image/JPG', 'image/X-PNG', 'image/PNG', 'image/png', 'image/x-png');
		
		if (in_array($_FILES['upload']['type'], $allowed)) {
			// Move the file over
			if (move_uploaded_file($_FILES['upload']['tmp_name'], "../uploads/{$_FILES['upload']['name']}")) {
				echo '<p><em>The file has been uploaded!</em></p>';
			}
		} else {
			echo '<p class="error">Please upload a JPEG or PNG image.</p>';
		}
	}
	
	// Check for an error:
	if ($_FILES['upload']['error'] > 0) {
		echo '<p class="error">The file could not be uploaded because: <strong>';
		
		// Print the error message based upon the error.
		switch ($_FILES['upload']['error']) {
			case 1:
				print 'The file exceeds the upload_max_filesize setting in php.ini.';
				break;
			case 2:
				print 'The file exceeds the MAX_FILE_SIZE setting in the HTML form.';
				break;
			case 3:
				print 'The file was only partially uploaded.';
				break;
			case 4:
				print 'No file was uploaded.';
				break;
			case 5:
				print 'No temporary folder was available.';
				break;
			case 6:
				print 'Unable to write to the disk.';
				break;
			case 7:
				print 'File upload stopped.';
				break;
			default:
				print 'A system error occurred.';
				break;
		}
		print '</strong></p>';
	}
	
	// Delete the file if it still exists:
	if (file_exists($_FILES['upload']['tmp_name']) && is_file($_FILES['upload']['tmp_name'])) {
		unlink($_FILES['upload']['tmp_name']);
	}
}
?>

<form enctype="multipart/form-data" action="upload_image.php" method="post">
	<input type="hidden" name="MAX_FILE_SIZE" value="524288" >
	<fieldset><legend>Select a JPEG or PNG image of 512KB or smaller to be uploaded:</legend>
	
	<p><b>File:</b><input type="file" name="upload" /></p>
	
	</fieldset>
	
	<div align="center"><input type="image" name="submit" src="http://www.premierptsolutions.com/images/submit_button.jpg" value="Submit" /></div>
	
	<input type="hidden" name="submitted" value="TRUE" />	
</form>
<?php
include('includes/footer.html');
?>