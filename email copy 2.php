<?php # Script 10.1 - email.php

// Retrieves all the records from the users table
// This new version links to edit and delete pages.

$page_title = 'Contact Me';
include('includes/header.html');
echo '<h1>Contact Me</h1>';

// Set default timezone
date_default_timezone_set('America/New_York');

if (isset($_POST['submitted'])) {
	// form validation
	if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['comments'])) {
		// create the body
		$body = "Name:
		{$_POST['name']}\n\nComments:
		{$_POST['comments']}";
		
		$body = wordwrap($body, 70);
		
		// Send the email:
		mail('jmorton429@gmail.com', 'Contact Form Submission', $body, "From: {$_POST['email']}");
		
		// Print a message:
		echo '<p><em>Thank you for contacting me at ' . date('g:i a(T)') . ' on ' . date('l F j, Y') . '. I will reply some day.</em></p>';
		
		// How long did it take?
		echo '<p><strong>It took ' . (time() - $_POST['start']) . ' seconds for you to complete and submit the form.</strong></p>';
		$_POST = array();
	} else {
		echo '<p style="font-weight: bold; color: #C00">Please fill out the form completely.</p>';
	}
}
?>

<p>Please fill out this form to contact me.</p>

<form action="email.php" method="post">
	<p>Name: <input type="text" name="name" size="30" maxlength="60" value="<?php if(isset($_POST['name'])) echo $_POST['name']; ?>" /></p>
	<p>Email Address: <input type="text" name="email" size="30" maxlength="80" value="<?php if(isset($_POST['email'])) echo $_POST['email']; ?>" /></p>
	<p>Comments: <textarea name="comments" rows="5" cols="30"><?php if(isset($_POST['comments'])) echo $_POST['comments']; ?></textarea></p>
	<p><input type="image" name="submit" src="http://mundu.com/im/images/send-button.png" value="Send!" /></p>
	<input type="hidden" name="start" value="<?php echo time(); ?>"/>
	<input type="hidden" name="submitted" value="TRUE" />
</form>

<?php
include('includes/footer.html');
?>