<?php # Script 11.13 - loggedin.php #3
// The user is redirected here from login.php

session_start(); // Start the session

// If no session value is present, redirect the user
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']))) {
	// Need the functions to create an absolute url:
	require_once('includes/login_functions.inc.php');
	$url = absolute_url();
	header("Location: $url");
	exit();
}

// Set the page title and include the header
$page_title = 'Logged In!';
include('includes/header.html');

// Print a customized message:
echo "<h1>Logged In!</h1>
<p>You are now logged in, {$_SESSION['first_name']}!</p>
<p><a href=\"logout.php\"><img src=\"http://www.kalymnos-hotels.com/Kalymnos-Climbing-Festival/members/styles/compact/images/en/ButtonLogout.gif\"></a></p>";

include('includes/footer.html');
?>