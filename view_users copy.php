<?php # Script 8.4 - view_users.php

// Retrieves all the records from the users table

$page_title = 'View the Current Users';
include('includes/header.html');

echo '<h1>Registered Users</h1>';

// Connect to the database
require_once('includes/mysqli_connect.php');

// Make the query
$q = "select concat(last_name, ', ', first_name) as name, date_format(registration_date, '%M %d, %Y') as dr from users order by registration_date asc";
$r = @mysqli_query ($dbc, $q); // Run query

if ($r) { // If it ran, display the records
	echo '<table align="center" cellspacing="3" cellpadding="3" width="75%"><tr><td align="left"><b>Name</b></td><td align="left"><b>Date Registered</b></td></tr>';
	
	while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
		echo '<tr><td align="left">' . $row['name'] . '</td><td align="left">' . $row['dr'] . '</td></tr>';
	}
	
	echo '</table>';
	
	mysqli_free_result ($r); // Free up resources
} else {
	echo '<p class="error">The current users could not be retrieved. Oops!</p>';
	// Debugging message
	echo '<p>' . mysqli_error($dbc) . '<br /><br />Query: ' . $q . '</p>';
}

mysqli_close($dbc);

include('includes/footer.html');
?>