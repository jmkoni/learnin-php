<?php # Script 13.2 - matches.php

$page_title = 'Testing PCRE';

include ('includes/header.html');
include ('includes/report_errors.php');

if (isset($_POST['submitted'])) {
	
	// Trim the strings
	$pattern = trim($_POST['pattern']);
	$subject = trim($_POST['subject']);
	
	// Print a caption:
	echo "<p>The result of checking<br /><b>$pattern</b><br />against<br />$subject<br />is ";
	// regex to test an email: /^[\w.-]+@[\w.-]+\.[A-Za-z]{2,6}\r?$/m
	
	// Test:
	if (preg_match_all($pattern, $subject, $matches)) {
		echo 'TRUE!</p>';
		// Print the matches
		echo '<pre>' . print_r($matches, 1) . '</pre><br />';
	} else {
		echo 'FALSE!</p>';
	}
}
?>
<form action="matches.php" method="post">
	<p>Regular Expression Pattern: <input type="text" name="pattern" value="<?php if (isset($pattern)) echo $pattern; ?>" size="30" /></p>
	<p>Test Subject: <textarea name="subject" rows="5" cols="30" value="<?php if (isset($subject)) echo $subject; ?>" size="30" ></textarea></p>
	<input type="submit" name="submit" value="Test!" />
	<input type="hidden" name="submitted" value="TRUE" />
</form>
<?php
include ('includes/footer.html');
?>